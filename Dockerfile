FROM php:8.2-cli

ENTRYPOINT ["/usr/local/bin/php"]
CMD ["-i"]
