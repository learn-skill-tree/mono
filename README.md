# Leavis

Leavis is a e-learning platform. Leavis is special because it enables users to see their complete learning roadmap.
You can see and define your personal learning vision.

## Description

In school you often learn skills without knowing what you will need that skill for in the long term.
In Leavis you can:

- Explore all available skills and long term goals (e.g. Astronaut, Physician, CEO)
- Set your personal learning goals
- Work on your skills and track progress

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

TODO (screenshots etc.)

## Installation
TODO

## Support
TODO

## Roadmap
TODO

## Contributing
TODO

## Authors and acknowledgment
TODO

## License
TODO

## Project status
Working on first release.
