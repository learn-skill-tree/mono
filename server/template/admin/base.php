<?php

$tailwindConfiguration = file_get_contents(__DIR__ . '/tailwind_config.json');

return _document(
    _html(
        _head(
            _meta(['charset' => 'utf-8']),
            _title('Leavis Administration'),
            _raw("
<script src='https://unpkg.com/htmx.org@1.9.5' integrity='sha384-xcuj3WpfgjlKF+FXhSQFQ0ZNr39ln+hwjN3npfM9VBnUskLolQAcN80McRIVOPuO' crossorigin='anonymous'></script>
<script src='https://cdn.tailwindcss.com'></script>
<script>
    tailwind.config = $tailwindConfiguration
</script>   
            "),
        ),
        _body(
            _block(
                'body',
                fn() => _wrapper(),
            )
        )
    )
);