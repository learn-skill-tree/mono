<?php

_extend('body', fn() => _form(
    _include('admin/components/forms/vertical-input-group.php', [
        'inputAttributes' => [
            'id' => 'login-email',
            'type' => 'email',
            'name' => 'email',
            'placeholder' => 'mr.white@breakingbad.com',
        ],
        'label' => 'E-Mail',
    ])
));

return _include('admin/base.php');