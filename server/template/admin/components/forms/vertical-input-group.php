<?php

/**
 * @var string $label
 * @var array $inputAttributes
 */

assert(is_string($label));
assert(is_array($inputAttributes));
if (!isset($labelAttributes)) {
    $labelAttributes = [];
} else {
    assert(is_array($labelAttributes));
}
$labelAttributes['for'] = $inputAttributes['id'];
$labelAttributes['class'] = 'block text-sm font-medium leading-6 text-gray-900';

return _div(['class' => 'components--forms--vertical-input-group'],
    _label($labelAttributes, $label),
    _div(['class' => 'relative mt-2 rounded-md shadow-md'],
        _div(['class' => 'pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3'],
            _span(['class' => 'text-gray-500 sm:text-sm'], '$'),
        ),

    )
);