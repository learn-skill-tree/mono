<?php

_extend('body', fn() => _div(
    _a(['href' => '/admin/login'], 'Admin Login'),
    _a(['href' => '/app'], 'App'),
));

return _include('page/base.php');