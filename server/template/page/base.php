<?php

return _document(
    _html(
        _head(
            _meta(['charset' => 'utf-8']),
            _title('Leavis Server'),
        ),
        _body(
            _block(
                'body',
                fn() => _wrapper(),
            )
        )
    )
);