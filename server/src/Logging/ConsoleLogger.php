<?php

namespace Leavis\Logging;

use Psr\Log\LoggerInterface;

class ConsoleLogger implements LoggerInterface
{
    public function emergency(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function alert(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function critical(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function error(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function warning(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function notice(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function info(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function debug(\Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

    public function log($level, \Stringable|string $message, array $context = []): void
    {
        echo ($message . json_encode($context));
    }

}