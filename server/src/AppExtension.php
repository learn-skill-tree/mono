<?php


namespace Leavis;


use Amp\Mysql\MysqlConfig;
use Amp\Mysql\MysqlConnection;
use Amp\Mysql\MysqlConnectionPool;
use League\Route\RouteGroup;
use League\Route\Router;
use Leavis\Actions\Admin\Login;
use Leavis\Actions\Page\GetIndex;
use Leavis\Configuration\DatabaseConfigurationInterface;
use Leavis\Logging\ConsoleLogger;
use Leavis\Migration\MigrationStateStorage;
use Phespro\Phespro\Extensibility\AbstractExtension;
use Phespro\Phespro\Http\Middlewares\AjaxOnlyMiddleware;
use Phespro\Phespro\Kernel;
use Phespro\Phespro\Migration\MigrationStateStorageInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class AppExtension extends AbstractExtension
{
    function boot(Kernel $kernel): void
    {
        $kernel->decorate(
            'template_dirs',
            fn (ContainerInterface $c, array $inner) => array_merge($inner, [__DIR__ . '/../template']),
        );

        $kernel->add(MysqlConnection::class, function(Kernel $kernel){
            $config = $kernel->getObject(DatabaseConfigurationInterface::class);

            $config = new MysqlConfig(
                host: $config->getHost(),
                port: $config->getPort(),
                user: $config->getUsername(),
                password: $config->getPassword(),
                database: $config->getDatabaseName(),
            );

            return new MysqlConnectionPool($config);
        }) ;

        $kernel->decorate(MigrationStateStorageInterface::class, fn() => new MigrationStateStorage(
            $kernel->get(MysqlConnection::class),
        ));

        $kernel->decorate(LoggerInterface::class, function() {
            return new ConsoleLogger();
        });

        Actions\_Services::register($kernel);
        Configuration\_Services::register($kernel);
        Migrations\_Services::register($kernel);
    }

    function bootHttp(Kernel $kernel, Router $router): void
    {
        $router->get('/', GetIndex::class);

        $router->get('/admin/login', Login::class);

        $router->group('/admin/components', function(RouteGroup $group) {

        })->middleware(new AjaxOnlyMiddleware);

        $router->group('/api', function(RouteGroup $group) {
//            $group->get('')
        });
    }
}
