<?php

namespace Leavis\Migration;

use Amp\Mysql\MysqlConnection;
use Phespro\Phespro\Migration\MigrationStateStorageInterface;
use function Functional\first;
use function Functional\map;

readonly class MigrationStateStorage implements MigrationStateStorageInterface
{
    public function __construct(
        protected MysqlConnection $mysqlConnection
    )
    {
    }

    public function add(string $id): void
    {
        $this->mysqlConnection->prepare("
            INSERT INTO migration(id)
            VALUES (:id)
        ")->execute(['id' => $id]);
    }

    public function contains(string $id): bool
    {
        $stmt = $this->mysqlConnection->prepare("SELECT COUNT(*) FROM migration WHERE id = :id");
        $result = $stmt->execute(['id' => $id]);
        return !!first($result->fetchRow());
    }

    public function getPassed(): iterable
    {
        return map(
            $this->mysqlConnection->query("SELECT id FROM migration"),
            fn(array $row) => $row['id'],
        );
    }

    public function prepareDataStructures(): void
    {
        $this->mysqlConnection->execute("
            CREATE TABLE IF NOT EXISTS migration (
                id VARCHAR(255) PRIMARY KEY
            );
        ");
    }
}