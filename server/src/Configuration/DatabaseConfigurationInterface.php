<?php

namespace Leavis\Configuration;

interface DatabaseConfigurationInterface
{
    public function getHost(): string;
    public function getPort(): int;
    public function getDatabaseName(): string;
    public function getUsername(): string;
    public function getPassword(): string;
}