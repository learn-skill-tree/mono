<?php

namespace Leavis\Configuration;

readonly class DatabaseConfiguration implements DatabaseConfigurationInterface
{
    public function getHost(): string
    {
        $result = getenv('LEAVIS_MYSQL_HOST');
        if (empty($result)) {
            throw new \Exception("Missing environment variable 'LEAVIS_MYSQL_HOST'");
        }
        return $result;
    }

    public function getPort(): int
    {
        $result = getenv('LEAVIS_MYSQL_PORT');
        if (empty($result)) {
            throw new \Exception("Missing environment variable 'LEAVIS_MYSQL_PORT'");
        }
        return $result;
    }

    public function getDatabaseName(): string
    {
        $result = getenv('LEAVIS_MYSQL_DATABASE');
        if (empty($result)) {
            throw new \Exception("Missing environment variable 'LEAVIS_MYSQL_DATABASE'");
        }
        return $result;
    }

    public function getUsername(): string
    {
        $result = getenv('LEAVIS_MYSQL_USER');
        if (empty($result)) {
            throw new \Exception("Missing environment variable 'LEAVIS_MYSQL_USER'");
        }
        return $result;
    }

    public function getPassword(): string
    {
        $result = getenv('LEAVIS_MYSQL_PASS');
        if (empty($result)) {
            throw new \Exception("Missing environment variable 'LEAVIS_MYSQL_PASS'");
        }
        return $result;
    }



}