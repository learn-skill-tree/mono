<?php

namespace Leavis\Configuration;

use Phespro\Phespro\Kernel;

final readonly class _Services
{
    public static function register(Kernel $kernel): void
    {
        $kernel->add(DatabaseConfigurationInterface::class, fn() => new DatabaseConfiguration);
    }
}