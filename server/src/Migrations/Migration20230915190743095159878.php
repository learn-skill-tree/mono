<?php

namespace Leavis\Migrations;

use Amp\Mysql\MysqlConnection;
use Phespro\Phespro\Migration\MigrationInterface;

readonly final class Migration20230915190743095159878 implements MigrationInterface
{
    public function __construct(
        private MysqlConnection $connection,
    )
    {
    }

    function getId(): string
    {
        return '20230915190743095159878';
    }

    function getDescription(): string
    {
        return 'InitialMigration';
    }

    function execute(): void
    {
        $commands = [
            "
            CREATE TABLE user (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                email VARCHAR(255) NOT NULL   
            );
            ",
            "
            CREATE TABLE language (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                iso_639_3_code VARCHAR(255) NOT NULL
            );
            ",
            "
            CREATE TABLE directory (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                path VARCHAR(255) NOT NULL,
                parent_id INT UNSIGNED,
                CONSTRAINT fk__directory__directory_parent FOREIGN KEY (parent_id) REFERENCES directory(id) ON DELETE CASCADE
            );
            ",
            "
            CREATE TABLE directory_translation (
                language_id INT UNSIGNED NOT NULL,
                directory_id INT UNSIGNED NOT NULL,
                name VARCHAR(255) NOT NULL,
                CONSTRAINT fk__directory_translation__language FOREIGN KEY (language_id) REFERENCES language(id),
                CONSTRAINT fk__directory_translation__directory FOREIGN KEY (directory_id) REFERENCES directory(id),
                PRIMARY KEY (language_id, directory_id)
            );
            ",
            "
            CREATE TABLE skill (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT
            );
            ",
            "
            CREATE TABLE skill_translation (
                language_id INT UNSIGNED NOT NULL,
                skill_id INT UNSIGNED NOT NULL,
                name VARCHAR(255) NOT NULL,
                description LONGTEXT NOT NULL,
                CONSTRAINT fk__skill_translation__language FOREIGN KEY (language_id) REFERENCES language(id),
                CONSTRAINT fk__skill_translation__skill FOREIGN KEY (skill_id) REFERENCES skill(id),
                PRIMARY KEY (language_id, skill_id)
            );
            ",
            "
            CREATE TABLE template (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT
            );
            ",
            "
            CREATE TABLE template_translation (
                language_id INT UNSIGNED NOT NULL,
                template_id INT UNSIGNED NOT NULL,
                name VARCHAR(255) NOT NULL,
                CONSTRAINT fk__template_translation__language FOREIGN KEY (language_id) REFERENCES language(id),
                CONSTRAINT fk__template_translation__template FOREIGN KEY (template_id) REFERENCES template(id),
                PRIMARY KEY (language_id, template_id)
            );
            ",
            "
            CREATE TABLE discipline (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                template_id INT UNSIGNED NOT NULL,
                CONSTRAINT fk__discipline__template FOREIGN KEY (template_id) REFERENCES template(id)
            );
            ",
            "
            CREATE TABLE discipline_translation (
                language_id INT UNSIGNED NOT NULL,
                template_id INT UNSIGNED NOT NULL,
                name VARCHAR(255) NOT NULL,
                CONSTRAINT fk__discipline_translation__language FOREIGN KEY (language_id) REFERENCES language(id),
                CONSTRAINT fk__discipline_translation__template FOREIGN KEY (template_id) REFERENCES template(id),
                PRIMARY KEY (language_id, template_id)
            );
            ",
            "
            CREATE TABLE grade (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                template_id INT UNSIGNED NOT NULL,
                CONSTRAINT fk__grade__template FOREIGN KEY (template_id) REFERENCES template(id)
            );
            ",
            "
            CREATE TABLE grade_translation (
                language_id INT UNSIGNED NOT NULL,
                grade_id INT UNSIGNED NOT NULL,
                CONSTRAINT fk__grade_translation__language FOREIGN KEY (language_id) REFERENCES language(id),
                CONSTRAINT fk__grade_translation__grade FOREIGN KEY (grade_id) REFERENCES grade(id),
                PRIMARY KEY (language_id, grade_id)
            );
            ",
            "
            CREATE TABLE template_skill (
                skill_id INT UNSIGNED NOT NULL,
                template_id INT UNSIGNED NOT NULL,
                discipline_id INT UNSIGNED,
                grade_id INT UNSIGNED,
                CONSTRAINT fk__template_skill__skill FOREIGN KEY (skill_id) REFERENCES skill(id) ON DELETE CASCADE,
                CONSTRAINT fk__template_skill__template FOREIGN KEY (template_id) REFERENCES template(id) ON DELETE CASCADE,
                CONSTRAINT fk__template_skill__discipline FOREIGN KEY (discipline_id) REFERENCES discipline(id) ON DELETE SET NULL,
                CONSTRAINT fk__template_skill__grade FOREIGN KEY (grade_id) REFERENCES grade(id) ON DELETE SET NULL,
                PRIMARY KEY (skill_id, template_id)
            );
            ",
            "
            CREATE TABLE vision (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                template_id INT UNSIGNED NOT NULL,
                user_id INT UNSIGNED NOT NULL,
                name VARCHAR(255) NOT NULL,
                CONSTRAINT fk__vision__template FOREIGN KEY (template_id) REFERENCES template(id),
                CONSTRAINT fk__vision__user FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
            );
            ",
            "
            CREATE TABLE skill_status (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                vision_id INT UNSIGNED NOT NULL,
                skill_id INT UNSIGNED NOT NULL,
                status ENUM('open', 'done') NOT NULL DEFAULT 'open',
                CONSTRAINT fk__skill_status__vision FOREIGN KEY (vision_id) REFERENCES vision(id) ON DELETE CASCADE,
                CONSTRAINT fk__skill_status__skill FOREIGN KEY (skill_id) REFERENCES skill(id) ON DELETE CASCADE
            );"
        ];

        foreach($commands as $command) {
            $this->connection->execute($command);
        }
    }
}
