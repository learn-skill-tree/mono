<?php

namespace Leavis\Migrations;

use Amp\Mysql\MysqlConnection;
use Phespro\Phespro\Kernel;

class _Services
{
    public static function register(Kernel $kernel): void
    {
        $kernel->add(
            Migration20230915190743095159878::class,
            fn() => new Migration20230915190743095159878(
                $kernel->get(MysqlConnection::class)
            ),
            ['migration']
        );
    }
}