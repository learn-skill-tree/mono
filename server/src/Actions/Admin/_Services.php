<?php

namespace Leavis\Actions\Admin;

use Phespro\Phespro\Kernel;

readonly final class _Services
{
    public static function register(Kernel $kernel): void
    {
        $kernel->add(Login::class, fn() => new Login);
    }
}