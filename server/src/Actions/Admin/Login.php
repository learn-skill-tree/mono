<?php

namespace Leavis\Actions\Admin;

use Phespro\Phespro\NoTee\NoTeeTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Login
{
    use NoTeeTrait;

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        return $this->renderResponse('admin/login.php');
    }
}