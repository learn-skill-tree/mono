<?php

namespace Leavis\Actions;

use Phespro\Phespro\Kernel;

class _Services
{
    public static function register(Kernel $kernel): void
    {
        Admin\_Services::register($kernel);
        Api\_Services::register($kernel);
        Page\_Services::register($kernel);
    }
}