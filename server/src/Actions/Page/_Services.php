<?php

namespace Leavis\Actions\Page;

use Phespro\Phespro\Kernel;

final readonly class _Services
{
    public static function register(Kernel $kernel): void
    {
        $kernel->add(GetIndex::class, fn() => new GetIndex);
    }
}