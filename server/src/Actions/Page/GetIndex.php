<?php

namespace Leavis\Actions\Page;

use Phespro\Phespro\NoTee\NoTeeTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class GetIndex
{
    use NoTeeTrait;

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        return $this->renderResponse('page/index.php');
    }
}