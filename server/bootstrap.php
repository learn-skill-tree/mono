<?php

namespace Phespro;

use Leavis\AppExtension;
use NoTee\NoTeeInterface;
use Phespro\Phespro\Configuration\FrameworkConfiguration;
use Phespro\Phespro\Kernel;
use Phespro\ReactHttpServerExtension\ReactExtension;

require __DIR__ . '/vendor/autoload.php';

$kernel = new Kernel([
    ReactExtension::class,
    AppExtension::class,
]);

$kernel->get(NoTeeInterface::class)->enableGlobal();
